# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./matthieu/matthieu.nix
      ./services.nix
      ./sound.nix
    ];

  nix.readOnlyStore = false; # To make the store compressible via btrfs

  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.permittedInsecurePackages = [
    "p7zip-16.02"
    "openssl-1.0.2u"
  ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;


    kernelModules = [ "fuse"  ];

    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
  };

  environment = {
    systemPackages = with pkgs; [
      # utils
      htop sudo pciutils lshw python3Full fuse sshfs-fuse
      docker-compose acpi usbutils xorg.xbacklight xbindkeys libinput
      borgbackup openvpn
    ];
  };

  time.timeZone = "Europe/Paris";


  networking = {
    networkmanager = {
      enable = true;
    };
    hostName = "nixstation";
  };

  virtualisation.docker.enable = true;


  # SOUND
  hardware.keyboard.zsa.enable = true;
  hardware.bluetooth.enable = true;

  # TOUCHPAD / TRACKPOINT
  hardware.trackpoint = {
    enable = true;
    device = "AlpsPS/2 ALPS DualPoint Stick";
    sensitivity = 50;
    speed = 25;
  };

  hardware.acpilight.enable = true;

  # KEYBOARD / FONT
  console.font = "Lat2-Terminus16";
  i18n = {
    defaultLocale = "fr_FR.UTF-8";
  };

  fonts = {
    fontDir.enable = true;
    fonts = with pkgs; [
      jetbrains-mono
      fira-code
      iosevka
    ];
  };
  services = {
    dbus = {
      enable = true;
      packages = with pkgs; [
        gnome3.dconf
      ];
    };
    udisks2.enable = true;
    illum.enable = true;
    openssh = {
      enable = true;
      passwordAuthentication = false;
    };
    printing = {
      enable = true;
      drivers = with pkgs; [ gutenprint hplip ];
    };
    gvfs.enable = true;
    xserver = {
      enable = true;
      windowManager = {
        awesome = {
          enable = true;
          luaModules = with pkgs; [
            lua52Packages.vicious
          ];
        };
      };
      displayManager = {
        lightdm = {
          enable = true;
        };
      };
      # keyboard
      layout = "fr,us";
      xkbOptions = "compose:prsc";
      # # resolution
      # dpi = 96;
      # monitorSection = "DisplaySize 1016 571";
      # # touchpad
      # libinput = {
      #   enable = true;
      #   tapping = false;
      # };
    };
  };

  programs = {
    light.enable = true;
    bash.enableCompletion = true;
    adb.enable = true;
    java.enable = true;
  };

  networking.firewall.enable = true;
  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [ 445 139 24800];
  networking.firewall.allowedUDPPorts = [ 137 138 ];

  hardware = {
    opengl = {
      driSupport32Bit = true;
      extraPackages32 = with pkgs.pkgsi686Linux; [ libva ] ++ lib.optionals config.services.pipewire.enable [ pipewire ];
    };
    logitech.wireless = {
      enable = true;
      enableGraphical = true;
    };
  };

  programs.dconf.enable = true;

  system.stateVersion = "20.03"; # Did you read the comment?
}
