{ config, pkgs, ... }:

let
  VPN_USERNAME = import ./secrets/repose.nix;
  VPN_PASSWORD = import ./secrets/unfumed.nix;
in {

  services = {
    logind.extraConfig = ''
      HandleLidSwitch=suspend
      HandlePowerKey=suspend
    '';

    xserver = {
      wacom.enable = true;
      libinput.enable = true;
    };

    postgresql = {
      enable = true;
      package = pkgs.postgresql_13;
      ensureDatabases = [ "mulchlabs" ];
      ensureUsers = [
        {
          name = "mulchlabs";
          ensurePermissions = {
            "DATABASE mulchlabs" = "ALL PRIVILEGES";
          };
        }
      ];
    };

    openssh.enable = true;

    influxdb = {
      enable = true;
      dataDir = "/tmp/influxdb";
    };

    openvpn.servers = {
      blah-biddy-bloo-blah = {
        config = ''${builtins.readFile secrets/unhairs.conf}'';
        autoStart = false;
        authUserPass = {
          username = "${VPN_USERNAME}";
          password = "${VPN_PASSWORD}";
        };
        updateResolvConf = true;
      };
    };
  };

}
