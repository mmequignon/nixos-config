{ config, pkgs, ... }:

let
  monitoring_webservice = import ./monitoring_webservice/default.nix;
in {

  environment.systemPackages = [
    monitoring_webservice
  ];

  systemd.services = {
    monitoring_webservice = {
      description = "Monitoring Webservice";
      wantedBy = [
        "multi-user.target"
      ];
      after = [
        "network.target"
      ];
      script = ''${monitoring_webservice}/bin/influxdb-api'';
      serviceConfig = {
        User = "influxdb";
      };
      environment = {
        PORT = "5678";
      };
    };
  };
}
