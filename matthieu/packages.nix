{ config, pkgs, ...}:

let
  git-autoshare = with pkgs.python3Packages; buildPythonPackage rec {
    pname = "git-autoshare";
    version = "1.0.0b4";
    src = with pkgs.python3Packages; fetchPypi {
      inherit pname version;
      sha256 = "102c605130b05bdce8e6a02e628bebd8d69c098d00e5b7ffdf5d60469bb69dd1";
    };
    checkInputs = [ setuptools_scm ];
    propagatedBuildInputs = [ pyyaml appdirs click ];
  };
in {
  users.users.matthieu.packages = with pkgs; [
    ## PASSWORDS
    pwgen mkpasswd
    ## GIT
    nix-prefetch-git nix-prefetch-github 
    git-crypt gitAndTools.hub gitAndTools.lab git-autoshare
    ## LISP
    # sbcl rlwrap lispPackages.quicklisp
    ## MAIL
    xapian notmuch-addrlookup 
    ## MEDIA
    imagemagick pdftk peek xournal youtube-dl zim mpv audacity spotdl
    digikam ffmpeg
    ## ERGODOX
    wally-cli
    ## SHELL
    pastebinit nethogs xorg.xev xorg.xmodmap tree ack silver-searcher curl
    mc unzip wget lm_sensors file zip rsync sysfsutils ext4magic
    p7zip unrar jmtpfs fsarchiver compsize tcpdump cifs-utils bind
    terminator
    ## GRAPHICAL
    peek xfce.thunar nicotine-plus transmission-gtk pavucontrol
    ## CHAT
    weechat tdesktop
    ## BROWSERS
    nyxt firefox chromium surf
    ## TOOLS
    gcc-unwrapped gcc gnumake i3lock-fancy polkit_gnome binutils steam steam-run
    ## code
    nixfmt gcc_multi virtualenv
  ];
  # Nicotine
  networking.firewall.allowedTCPPorts = [ 17000 ];
}
