{ config, pkgs, ... }:

let
  FWZTE_MATTHIEU_ADDRESS = import ./secrets/malignly.nix;
  GPG_SIGNING_KEY = import ./secrets/indented.nix;
in {
  home-manager.users.matthieu = {
    xdg.configFile = {
      "git-autoshare/repos.yml".source = ./dotfiles/git_autoshare_repos.yml;
    };

    home = {
      sessionVariables = {
        GIT_AUTOSHARE_GIT_BIN = "${pkgs.git}/bin/git";
      };
    };
    programs.git = {
      package = pkgs.gitAndTools.gitFull;
      enable = true;
      delta = {
        enable = true;
        options = {
          features = "side-by-side";
        };
      };
      userEmail = "${FWZTE_MATTHIEU_ADDRESS}";
      signing = {
        key = "${GPG_SIGNING_KEY}";
        signByDefault = true;
      };
      aliases = {
        glog = "log --graph --decorate";
        ci = "commit";
        st = "status";
      };
      extraConfig = {
        core = {
          editor = "nvim";
          pager = "";
          excludesfile = "~/.gitignore";
        };
        pull = {
          rebase = true;
        };
        user = {
          name = "Mmequignon";
        };
        hub = {
          protocol = "ssh";
        };
        init = {
          defaultBranch = "master";
        };
      };
    };
  };

}
