source /etc/nixos/matthieu/dotfiles/nvim/vim/options.vim
source /etc/nixos/matthieu/dotfiles/nvim/vim/mappings.vim
source /etc/nixos/matthieu/dotfiles/nvim/vim/airline.vim
source /etc/nixos/matthieu/dotfiles/nvim/vim/nerdtree.vim
source /etc/nixos/matthieu/dotfiles/nvim/vim/undotree.vim
source /etc/nixos/matthieu/dotfiles/nvim/vim/tagbar.vim
source /etc/nixos/matthieu/dotfiles/nvim/vim/fzf.vim

"let g:coq_settings = {
"\ 'xdg': 'true',
"\ 'autostart': 'shut-up',
"\ 'clients': {
"\   'tree_sitter.enabled': 'false',
"\   'tmux.enabled': 'false',
"\   'third_party.enable': 'false',
"\  }
"\}
