" nnoremap <F4> :NvimTreeFindFileToggle<CR>
nnoremap <silent><expr> <F4> g:NERDTree.IsOpen() ? ":NERDTreeClose\<CR>" : bufname("%") == "" ? ":NERDTreeCWD\<CR>" : ":NERDTreeFind\<CR>"
nnoremap <silent> <F5> :TagbarToggle <CR> \| :TagbarTogglePause<CR>
nnoremap <silent> <F6> :UndotreeToggle<cr>
"  vnoremap <F9> zf
nnoremap <silent> <F12> :nohl<CR>
nnoremap <C-t> :tabnew<CR>

nnoremap K :Ag <C-R><C-W>
vnoremap K y:Ag <C-r>"
vnoremap <Leader>p d:r! python -c 'import pprint; pprint.pprint(<C-r>"<C-r> , indent=4)' +\%c<CR>

"désactivation de la surbrillance de la dernière recherche ctrl + n
nnoremap <Leader>* :nohl<CR>

"classer avec leader + s
vnoremap <Leader>s :sort<CR>

"manipuler des blocs complets avec < et >
vnoremap < <gv
vnoremap > >gv

nnoremap <Leader>k :CloseHiddenBuffers<CR>

nnoremap <Leader>- yyp<c-v>$r-
nnoremap <Leader>+ yyp<c-v>$r+
nnoremap <Leader>_ yyp<c-v>$r_
nnoremap <Leader>= yyp<c-v>$r=

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

nmap <silent> <unique> + <Plug>nextvalInc
nmap <silent> <unique> - <Plug>nextvalDec

" tests
com! FormatXML :%!python3 -c "import xml.dom.minidom, sys; print(xml.dom.minidom.parse(sys.stdin).toprettyxml())"
nnoremap = :FormatXML<Cr>
