set nocompatible
set title

set encoding=utf-8
set fileencoding=utf-8

set number
set nowrap
set mouse=c
let mapleader=","
let maplocalleader=";"
set bs=2

syntax enable
set showmatch
let python_highlight_all = 1

set termguicolors
set background=dark
colorscheme selenized

set incsearch
set hlsearch
set ignorecase
set smartcase

match ErrorMsg /\s\+$\| \+\ze\t/

set expandtab           " enter spaces when tab is pressed
set tabstop=4           " use 4 spaces to represent tab
set softtabstop=4
set shiftwidth=4        " number of spaces to use for auto indent
set showcmd                     " show (partial) command in status line

set colorcolumn=80
set cul
set fileformat=unix
set hid

set noswapfile

set splitbelow
set splitright

set laststatus=2
autocmd StdinReadPre * let s:std_in=1
