{ config, pkgs, ... }:

let
  ROTATE_SCREEN_FILE_PATH = "${builtins.toString ./.}/bin/rotate_screen";
in {
  environment.systemPackages = with pkgs; [
    i3lock
  ];

  systemd.user.timers = {
    rotate_screen_timer = {
      enable = true;
      description = "Rotate Screen Timer";
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "*-*-* *:*:0/5";
        AccuracySec = "1s";
        Unit = "screen_rotate.service";
      };
    };
  };
  systemd.services = {
    i3lock = {
      description = "i3lock";
      before = [ "sleep.target" ];
      serviceConfig = {
        User = "matthieu";
        Type = "forking";
        Environment="DISPLAY=:0";
        ExecStart=''${pkgs.i3lock}/bin/i3lock'';
      };
      wantedBy = [ "sleep.target" ];
      enable = true;
    };
  };
  systemd.user.services = {
    screen_rotate = {
      path = [
        pkgs.python3Full
        pkgs.xorg.xinput
        pkgs.xorg.xrandr
        pkgs.xf86_input_wacom
      ];
      description = "Computes screen orientation every 5 secs and rotates it";
      environment = {
        DISPLAY = ":0";
      };
      serviceConfig = {
        Type = "oneshot";
        ExecStart= ''
          ${pkgs.python3Full}/bin/python3 ${ROTATE_SCREEN_FILE_PATH}
        '';
        Environment="DISPLAY=:0";
      };
      wantedBy = [ "graphical.target" ];
    };
  };
}
