{ config, pkgs, ...}:

let 
  BORG_PASSPHRASE = import ./secrets/tractility.nix;
  BACKUP_IP = import ./secrets/overspeed.nix;
  ROOT_PATH = "/home/matthieu/";
  BORG_REPO = "ssh://borg@${BACKUP_IP}:22/srv/share/borg/matthieu_home";
in {
  users.users.matthieu.packages = with pkgs; [
    borgbackup
  ];

  services.borgbackup.jobs.matthieu_home = {
    paths = [
      "${ROOT_PATH}Documents"
      "${ROOT_PATH}.gnupg"
    ];
    user = "matthieu";
    repo = "${BORG_REPO}";
    encryption = {
      mode = "keyfile";
      passphrase = "${BORG_PASSPHRASE}";
    };
    compression = "lzma";
    startAt = "daily";
  };

  home-manager.users.matthieu = {
    home.sessionVariables = {
      BORG_PASSPHRASE="${BORG_PASSPHRASE}";
      BORG_REPO="${BORG_REPO}";
    };
  };
}
