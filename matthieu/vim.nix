{ config, pkgs, ... }:

let 
  nvim_dotfiles_folder = "${builtins.toString ./.}/dotfiles/nvim";
in {
  home-manager.users.matthieu = {

    home.file = {
      ".vim/pylintrc".source = ./dotfiles/pylintrc;
      ".vim/colors/selenized.vim".source = ./dotfiles/selenized.vim;
      ".config/nvim/colors/selenized.vim".source = ./dotfiles/selenized.vim;
    };

    programs = {
      neovim = {
        enable = true;
        extraConfig = let
          filename = "~/Addresses.db";
        in ''
          source ${nvim_dotfiles_folder}/vimrc.vim
          let g:deoplete#enable_at_startup = 1
          autocmd CompleteDone * silent! pclose!
          let g:jedi#completions_enabled = 0
          if filereadable(expand("${filename}"))
            autocmd FileType mail call deoplete#disable()
            autocmd FileType mail :set complete+=k${filename}
            autocmd FileType mail :set iskeyword+=.-.
            autocmd FileType mail :set iskeyword+=@-@
            autocmd FileType mail :set iskeyword+=---
          endif
        '';
        extraPython3Packages = (ps: with ps; [
          pylint
          pynvim
          msgpack
          jedi
        ]);
        extraPackages = with pkgs; [
          # ctags
          universal-ctags # TODO: let's see if it is compatible
        ]; 
        plugins = with pkgs.vimPlugins; [
          ale
          fzf-vim
          nerdcommenter
          nerdtree
          tagbar
          ultisnips
          undotree
          vim-airline
          vim-airline-themes
          vim-devicons
          vim-nix
          vim-signify
          vim-surround
          vim-fugitive
          deoplete-nvim
          deoplete-jedi
        ];
        vimAlias = true;
        vimdiffAlias = true;
      };
    };
  };
}
