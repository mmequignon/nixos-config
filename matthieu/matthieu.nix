{ config, pkgs, ... }:

let
  authorized_keys = import ./secrets/incrust.nix;
in {
  imports = [
    # /home/matthieu/projects/home-manager/nixos
    <home-manager/nixos>
    ./services.nix
    ./zsh.nix
    ./gpg.nix
    ./mail.nix
    ./packages.nix
    ./vim.nix
    ./borg.nix
    ./obs.nix
    ./media_edition.nix
    ./home_manager.nix
    # ./git.nix
    # ./wm.nix
  ];

  nixpkgs.config.allowUnfree = true;


  users.extraUsers.matthieu = {
    shell = pkgs.zsh;
    extraGroups = [
      "wheel"
      "docker"
      "networkmanager"
      "audio"
      "jackaudio"
      "realtime"
      "fuse"
      "video"
      "plugdev"
    ];
    isNormalUser = true;
    openssh.authorizedKeys.keys = authorized_keys;
  };

}
