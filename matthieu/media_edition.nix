{ config, pkgs, ... }:

{
  users.users.matthieu.packages = with pkgs; [
    inkscape gimp kdenlive musescore dia krita libreoffice ardour harvid
  ];
}
