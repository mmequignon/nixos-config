{ config, pkgs, ... }:

let
  FWZTE_MATTHIEU_ADDRESS = import ./secrets/malignly.nix;
  GPG_SIGNING_KEY = import ./secrets/indented.nix;
  MPD_MUSIC_DIRECTORY = "/srv/share/music"; 
  MPD_HOST = "6600";
  NEWSBOAT_URLS = import ./secrets/beanstalk.nix;
  PET_TOKEN = import ./secrets/nondelegation.nix;
in {
  home-manager.users.matthieu = {
    xdg.userDirs.enable = true;

    gtk = {
      enable = true;
      theme = {
        name = "Adwaita-dark";
        package = pkgs.gnome3.gnome_themes_standard;
      };
    };


    xdg = {
      configFile = {
        "terminator/config".source = ./dotfiles/terminator.conf;
        "awesome".source = ./dotfiles/awesome;
        "mc".source = ./dotfiles/mc;
      };
    };

    home = {
      sessionPath = [
        "~/bin/"
      ];
      file = {
        ".XCompose".source = ./dotfiles/xcompose;
        "Addresses.db".source = ./secrets/elzevirian.db;
        "bin".source = ./bin;
      };
    };

    services = {
      flameshot.enable = true;
      caffeine.enable = true;
      blueman-applet.enable = true;
      network-manager-applet.enable = true;
      gammastep = {
        enable = true;
        tray = true;
        temperature.night = 4250;
        latitude = "48.1";
        longitude = "-1.7";
        #settings = {
          #redshift = {
            #brightness_night = "0.7";
          #};
        #};
      };
      unclutter.enable = true;
      # screenlocker TODO
      # xsuspender   TODO
    };

    programs = {
      feh.enable = true;     # TODO configuration
      zathura.enable = true; # TODO configuration
      firefox.enable = true;
      fzf = {
        enable = true;     # TODO configuration
        enableZshIntegration = true;
      };
      # gh.enable = true; # TODO configuration
      mpv.enable = true;     # TODO configuration ?
      # TODO ssh configuration
      # TODO htop + config
      # TODO rofi
      pet = {
        enable = true;
        selectcmdPackage = pkgs.fzf;
        settings = {
          General = {
            backend = "gitlab";
            editor = "nvim";
          };
          GitLab = {
            access_token = "${PET_TOKEN}";
            file_name = "pet-snippets.toml";
            id = "2196553";
            visibility = "public";
          };
        };
      };

      # octant.enable = true; # TODO install and test

      nix-index = {
        enable = true;
        enableZshIntegration = true;
      };

      git = {
        package = pkgs.gitAndTools.gitFull;
        enable = true;
        delta = {
          enable = true;
          options = {
            features = "side-by-side";
          };
        };
        userEmail = "${FWZTE_MATTHIEU_ADDRESS}";
        signing = {
          key = "${GPG_SIGNING_KEY}";
          signByDefault = true;
        };
        aliases = {
          glog = "log --graph --decorate";
          ci = "commit";
          st = "status";
        };
        extraConfig = {
          core = {
            editor = "vim";
            excludesfile = "~/.gitignore";
          };
          pull = {
            rebase = true;
          };
          user = {
            name = "Mmequignon";
          };
          hub = {
            protocol = "ssh";
          };
          init = {
            defaultBranch = "master";
          };
        };
      };

      tmux = {
        enable = true;
        extraConfig = builtins.readFile dotfiles/tmux.conf;
        plugins = with pkgs; [
          {
            plugin = tmuxPlugins.sidebar;
            extraConfig = "set -g @sidebar-tree-command 'tree -C'";
          }
        ];
      };

      newsboat = {
        enable = true;
        autoReload = false;
        urls = NEWSBOAT_URLS;
        extraConfig = builtins.readFile dotfiles/newsboat.conf;
      };

      ncmpcpp = {
        enable = true;
        bindings = [
          {key = "j"; command="scroll_down";}
          {key = "k"; command="scroll_up";}
          {key = "h"; command="previous_column";}
          {key = "l"; command="next_column";}
          {key = "L"; command="next_screen";}
          {key = "H"; command="previous_screen";}
          {key = "g"; command="move_home";}
          {key = "G"; command="move_end";}
        ];
        mpdMusicDir = "${MPD_MUSIC_DIRECTORY}";
        settings = {
          mpd_host = "192.168.1.47";
          mpd_port = "6600";
          mpd_crossfade_time = "5";
          discard_colors_if_item_is_selected = "yes";
          header_window_color = "black";
          volume_color = "cyan";
          state_line_color = "green";
          state_flags_color = "red";
          main_window_color = "default";
          color1 = "default";
          color2 = "black";
          current_item_inactive_column_prefix = "$(red)$r";
          current_item_inactive_column_suffix = "$/r$(end)";
          current_item_prefix = "$(red)$r";
          current_item_suffix = "$/r$(end)";
         progressbar_color = "green";
          visualizer_color = "cyan";
        };

      };

      autorandr = {
        enable = true;
        profiles = {
          "mobile" = {
            fingerprint = {
              eDP-1 = "00ffffffffffff0030e437040000000000170104951c107802bf059a59558e261d505400000001010101010101010101010101010101163680ba70380f4030203500149c1000001a000000000000000000000000000000000000000000fe004c4720446973706c61790a2020000000fe004c503132355746322d5350423200dd";
            };
            config = {
              eDP-1 = {
                enable = true;
                primary = true;
                position = "0x0";
                mode = "1920x1080";
              };
            };
            hooks.postswitch = ''
              xsetwacom --set 11 Rotate 0
              xinput --set-prop 9 175 1 0 0 0 1 0 0 0 1
            '';
          };
          "home" = {
            fingerprint = {
              eDP-1 = "00ffffffffffff0030e437040000000000170104951c107802bf059a59558e261d505400000001010101010101010101010101010101163680ba70380f4030203500149c1000001a000000000000000000000000000000000000000000fe004c4720446973706c61790a2020000000fe004c503132355746322d5350423200dd";
              HDMI-2 = "00ffffffffffff0026cd0476630000002b1c0103805022782aca95a6554ea1260f5054bd4b00d1c081808140950f9500b30081c00101dea370e0d4a0355000703a50204f3100001cef5170e0d4a0355000703a50204f3100001c000000fc00504c3334393057510a20202020000000fd0017501e632d000a2020202020200175020335f155101f0413031202110105140706161544454b4c595a23090707830100006a030c001000393c20000067d85dc401788803565e00a0a0a0295030203500204f3100001eb33900a080381f4030203a00204f3100001eef51b87062a0355080b83a00204f3100001c000000000000000000000000000000000000000055";
            };
            config = {
              eDP1 = {
                enable = false;
                primary = false;
              };
              HDMI-2 = {
                enable = true;
                primary = true;
                position = "0x0";
                mode = "3440x1440";
              };
            };
          };
        };
      };
    };

    manual.manpages.enable = true;

  };
}
