{ config, pkgs, lib, ...}:


{

  users.users.matthieu.packages = with pkgs; [
    swaylock grim
  ];

  systemd.services.swaylock = {
    description = "swaylock";
    partOf = ["graphical-session.target"];
    serviceConfig = {
      Type = "simple";
      ExecStart=''${pkgs.swayidle}/bin/swayidle -w \
        timeout 300 '${pkgs.swaylock}/bin/swaylock -f -c 000000' \
        timeout 600 '${pkgs.sway}/bin/swaymsg "output * dpms off"' \
        resume '${pkgs.sway}/bin/swaymsg "output * dpms on"' \
        before-sleep '${pkgs.swaylock}/bin/swaylock -f -c 000000'
      '';
    };
    wantedBy = [ "sway-session.target" ];
    enable = true;
  };

  home-manager.users.matthieu =
    let
      modifier = "Mod4";
      here = builtins.toString ./.;
    in {
      wayland.windowManager.sway = {
        enable = true;
        config = {
          modifier = "${modifier}";
          output = {
            "*" = {
              bg = "${here}/dotfiles/wallpaper.jpg fill";
            };
          };
          keybindings = lib.mkOptionDefault {
            "${modifier}+Shift+c" = "kill";
            "${modifier}+Shift+r" = "reload";
            "${modifier}+r" = "exec ${pkgs.rofi}/bin/rofi -show run";
            "${modifier}+d" = "mode resize";
            "${modifier}+Escape" = "workspace back_and_forth";
            #"${modifier}+Space" = "layout toggle";
            "${modifier}+Control+Shift+h" = "focus output left";
            "${modifier}+Control+Shift+j" = "focus output down";
            "${modifier}+Control+Shift+k" = "focus output up";
            "${modifier}+Control+Shift+l" = "focus output right";
            "${modifier}+Control+Shift+Alt+h" = "move window to output left";
            "${modifier}+Control+Shift+Alt+j" = "move window to output down";
            "${modifier}+Control+Shift+Alt+k" = "move window to output up";
            "${modifier}+Control+Shift+Alt+l" = "move window to output right";
          };
          bars = [];
          terminal = "${pkgs.terminator}/bin/terminator";
          bindkeysToCode = true;
          startup = [
            {command="waybar";}
          ];
          input = {
            "type:keyboard" = {
              xkb_options = "compose:prsc";
              xkb_layout = "fr";
            };
            # "ergodox identifier" = {
            #   xkb_layout = "us";
            # };
          };
        };
        extraConfig = ''
          font pango:monospace 0
          titlebar_border_thickness 0                                                     
          titlebar_padding 0
          exec [ -x $HOME/bin/tamefox ] && $HOME/bin/tamefox
          default_border none
        '';
      };

    programs.rofi = {
      enable = true;
      location = "top";
      theme = "solarized";
    };
    programs.waybar = {
      enable = true;
      style = builtins.readFile dotfiles/waybar-style.css;
      settings = [
        {
          position = "right";
          height = 50;
          modules-left = [ "clock#1" "clock#2" "battery" "memory" "cpu" "temperature" "pulseaudio" "tray" ];
          modules-center = [ "wlr/taskbar" ];
          modules-right = [ "sway/workspaces" ];
          modules = {
            "wlr/taskbar" = {
              icon-size = 20;
            };
            "clock#1" = {
              format = "{:%H:%M}";
            };
            "clock#2" = {
              format = "{:%Y-%m-%d}";
            };
            "memory" = {
              format = "{used:0.1f}G";
            };
            "cpu" = {
              format = "{usage}%";
            };
            "tray" = {
                icon-size = 20;
                spacing = 8;
            };
            #"battery"= {
              #interval = 10;
              #states = {
                #warning = 30;
                #critical = 15;
              #};
              #format = "{capacity}% {icon}";
              #format-icons = ["" "" "" "" ""];
            #};
            "sway/workspaces"= {
              disable-scroll = true;
              all-outputs = true;
              persistent_workspaces = {
                "1"= "[]";
                "2"= "[]";
                "3"= "[]";
                "4"= "[]";
                "5"= "[]";
                "6"= "[]";
                "7"= "[]";
                "8"= "[]";
                "9"= "[]";
              };
            };
          };
        }
      ];
    };
  };
}
