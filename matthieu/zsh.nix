{ config, pkgs, lib, ...}:

{
  environment.shells = [
    pkgs.bashInteractive pkgs.zsh
  ];

  home-manager.users.matthieu = {
    home.file.".zsh/completions/_hub".source = ./dotfiles/_hub;

    programs = {
      zsh = {
        enable = true;
        enableAutosuggestions = true;
        enableCompletion = true;
        shellAliases = {
          cpr = "rsync -a --info = progress2";
          ls = "ls -sh --color=auto ";
          ll = "ls -lh";
          lla = "ls -lah";
          llas = "ls -laSh";
          grep = "grep --color";
          tmux = "tmux -2";
        };
        sessionVariables = {
          PATH = "/home/matthieu/bin:$PATH";
          EDITOR = "nvim";
          TERM = "xterm-256color";
        };
        # autoload -U compinit && compinit
        initExtra = ''
            passgen() {
              pass generate "$1" && pass edit "$1";
            }
        '';
        oh-my-zsh.enable = true;
      };

      starship = {
        enable = true;
        enableZshIntegration = true;
        settings = {
          add_newline = false;
          line_break.disabled = true;
          username = {
            show_always = true;
            format= "[$user]($style)@";
          };
          directory = {
            style = "bold blue";
            truncate_to_repo = false;
          };
          hostname = {
            ssh_only = false;
            format = "[$hostname]($style) in ";
          };
          cmd_duration.disabled = true;
          git_status.disabled = true;
          format = lib.concatStrings [ "$username" "$hostname" "$directory" "$git_branch" "$git_state" ];
        };
      };
    };
  };
}
