{ config, pkgs, ... }:

{
  home-manager.users.matthieu = {
    programs.obs-studio = {
      enable = true;
      plugins = [];
    };
  };
}
