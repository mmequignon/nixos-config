Setup
=====

1. Configuration setup
    see : https://gist.github.com/alcol80/28671f6c07bea22e6761
2. Move /etc/nixos to /etc/nixos-bak
3. Git clone https://gitlab.com/mmequignon/nixos-config -b setup
    This branch contains only what's necessary to begin with, in order to
    decrypt secret files.
4. Configure GPG → TODO
5. git-crypt unlock
6. git fetch origin master && git checkout master
7. nix-build
